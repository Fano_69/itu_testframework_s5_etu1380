<%-- 
    Document   : listDept
    Created on : Oct 22, 2022, 4:15:27 PM
    Author     : Marshalls
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import ="java.util.ArrayList"%>
<%@page import ="model.Dept"%>
<%
   ArrayList<Dept> list = (ArrayList<Dept>)request.getAttribute("listDept");
    String num = (String)request.getAttribute("num");
    String boss = (String)request.getAttribute("boss");
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World XD!</h1>
        <div>Boss : <%=boss%></div>
        <div>Nombre : <%=num%></div>
        <table border="1">
            <tr>
                <th>ID</th>
                <th>Nom</th>
            </tr>
        <% for (Dept d : list){ %>
            <tr>
                <td><%=d.getIdDept()%></td>
                <td><%=d.getNomDept()%></td>
            </tr>
        <% } %>
        </table>
    </body>
</html>
