/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import generic.ModelView;
import generic.Path;
import java.util.ArrayList;

/**
 *
 * @author Marshalls
 */
public class Dept {
    int idDept;
    String nomDept;

    public Dept(int idDept, String nomDept) {
        this.idDept = idDept;
        this.nomDept = nomDept;
    }

    public int getIdDept() {
        return idDept;
    }

    public void setIdDept(int idDept) {
        this.idDept = idDept;
    }

    public String getNomDept() {
        return nomDept;
    }

    public void setNomDept(String nomDept) {
        this.nomDept = nomDept;
    }
    
    @Path(url = "list", klass="dept")
    public static ModelView getAll(){
        ArrayList<Dept> all = new ArrayList<Dept>();
        all.add(new Dept(1,"Appro"));
        all.add(new Dept(2,"Appro"));
        all.add(new Dept(3,"Appro"));
        int num = all.size();
        String boss = "Salama";
        
        ModelView val = new ModelView();
        val.setTarget("/listDept.jsp");
        val.addAttribute("listDept", all);
        val.addAttribute("num", num);
        val.addAttribute("Salama", salama);
        return val;
    }
}
